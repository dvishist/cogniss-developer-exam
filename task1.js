const jsonfile = require("jsonfile")
const randomstring = require("randomstring")
const got = require("got")

const outputFile = 'output2.json'

//reverse a string by reducing every character to a new string right to left
const reverseString = (str) => {
    return Array.from(str).reduce((newString, char) => char + newString, "")
}

//add 5 random letters and @gmail.com to a string
const buildEmail = (str) => {
    return str + randomstring.generate(5).toLowerCase() + "@gmail.com"
}

//read json file and map names to new emails
jsonfile.readFile('input2.json', (err, obj) => {
    if (err) console.log(err)

    //map names to emails
    //lowercase names to match email conventions
    const emailArray = obj.names.map(name => buildEmail(reverseString(name.toLowerCase())))

    // write to JSON file as emails property
    jsonfile.writeFile(outputFile, { emails: emailArray }, (err) => {
        if (err) console.log(err)
    })
})
