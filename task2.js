const jsonfile = require("jsonfile");
const randomstring = require("randomstring");
const got = require("got");

const inputURL = "http://www.nactem.ac.uk/software/acromine/dictionary.py?sf=";
const outputFile = "output3.json";

var output = {}
console.log("getting 10 acronyms");
output.definitions = [];
console.log("creating looping function")
const getAcronym = function () {
  var acronym = randomstring.generate(3).toUpperCase();
  got(inputURL + acronym).then(response => {
    console.log("got data for acronym", acronym);
    console.log("add returned data to definitions array");

    //sometimes acronyms return 0 definitions resulting in empty arrays, can filter out empty results using the following line
    // if (response.body !== '[]\n')
    //   output.definitions.push(JSON.parse(response.body));

    //convert response body from String to Object for more concise JSON file
    output.definitions.push(JSON.parse(response.body));

    //move JSON file write to else condition to wait for definitions length to be 10
    if (output.definitions.length < 10) {
      console.log("calling looping function again");
      getAcronym();
    } else {
      console.log("saving output file formatted with 2 space indenting");
      jsonfile.writeFile(outputFile, output, { spaces: 2 }, function (err) {
        console.log("All done!");
      });
    }
  }).catch(err => {
    console.log(err)
  })
}
console.log("calling looping function");
getAcronym();
